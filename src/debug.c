#include "common.h"

#define SAMELINE -100

void disassembleChunk(Chunk* chunk, const char* name) {
    printf("== %s ==\n", name);

    int lastLine = 0;
    for (int offset = 0; offset < chunk->count;) {
        int line = getLine(chunk, offset);
        if (line == lastLine) {
            line = SAMELINE;
        } else {
            lastLine = line;
        }
        if (disassembleInstruction(chunk, offset++, line)) {
            offset++;
        }
    }
}

static bool A_instruction(Instruction i, const char* name,
                          Chunk* UNUSED(chunk), int UNUSED(offset)) {
    printf("%-16s %d\n", name, A(i));
    return false;
}

static bool Aflag_instruction(Instruction i, const char* name,
                              Chunk* UNUSED(chunk), int UNUSED(offset)) {
    if (flag(i)) {
        printf("%-16s %d\n", name, A(i));
    } else {
        printf("%-16s nil\n", name);
    }
    return false;
}

static bool AB_instruction(Instruction i, const char* name,
                           Chunk* UNUSED(chunk), int UNUSED(offset)) {
    printf("%-16s %d %d\n", name, A(i), B(i));
    return false;
}

static bool ABC_instruction(Instruction i, const char* name,
                            Chunk* UNUSED(chunk), int UNUSED(offset)) {
    printf("%-16s %d %d %d\n", name, A(i), B(i), C(i));
    return false;
}

static bool ABKC_instruction(Instruction i, const char* name, Chunk* chunk,
                             int UNUSED(offset)) {
    printf("%-16s %d %d(%s) %d", name, A(i), B(i), flag(i) ? "const" : "reg",
           C(i));
    if (flag(i)) {
        printf(" : ");
        printValue(chunk->constants.values[B(i)]);
    }
    printf("\n");
    return false;
}

static bool ABCX_instruction(Instruction i, const char* name, Chunk* chunk,
                             int offset) {
    printf("%-16s %d %d %d %d\n", name, A(i), B(i), C(i),
           Ax(chunk->code[offset + 1]));
    return true;
}

static bool AeBx_instruction(Instruction i, const char* name, Chunk* chunk,
                             int offset) {
    i32 arg_eBx = flag(i) ? Ax(chunk->code[offset + 1]) : Bx(i);
    printf("%-16s %d %d\n", name, A(i), arg_eBx);
    return flag(i);
}

static bool AeBxK_instruction(Instruction i, const char* name, Chunk* chunk,
                              int offset) {
    i32 arg_eBx = flag(i) ? Ax(chunk->code[offset + 1]) : Bx(i);
    printf("%-16s %d %d : ", name, A(i), arg_eBx);
    printValue(chunk->constants.values[arg_eBx]);
    printf("\n");
    return flag(i);
}

static bool AsBx_instruction(Instruction i, const char* name,
                             Chunk* UNUSED(chunk), int UNUSED(offset)) {
    printf("%-16s %d %d\n", name, A(i), sBx(i));
    return false;
}

static bool Ax_instruction(Instruction i, const char* name, Chunk* chunk,
                           int UNUSED(offset)) {
    printf("%-16s %d : ", name, Ax(i));
    printValue(chunk->constants.values[Ax(i)]);
    printf("\n");
    return false;
}

static bool sAx_instruction(Instruction i, const char* name,
                            Chunk* UNUSED(chunk), int UNUSED(offset)) {
    printf("%-16s %d\n", name, sAx(i));
    return false;
}

int disassembleInstruction(Chunk* chunk, int offset, int line) {
    printf("%04d ", offset);
    if (line == SAMELINE) {
        printf("   | ");
    } else {
        printf("%4d ", line);
    }

    Instruction i = chunk->code[offset];

#define debugcase(name, format) \
    case name: \
        return format##_instruction(i, #name, chunk, offset)

    switch (op(i)) {
        debugcase(OPR_MOVE, AB);
        debugcase(OPR_LOADK, AeBxK);
        debugcase(OPR_LOADI, AsBx);
        debugcase(OPR_LOADNIL, A);
        debugcase(OPR_LOADBOOL, AB);
        debugcase(OPR_GETGLOBAL, AeBxK);
        debugcase(OPR_SETGLOBAL, AeBxK);
        debugcase(OPR_DEFGLOBAL, AeBxK);
        debugcase(OPR_GETUPVAL, AB);
        debugcase(OPR_SETUPVAL, AB);
        debugcase(OPR_GETATTR, AeBxK);
        debugcase(OPR_SETATTR, ABKC);
        debugcase(OPR_GETSUPER, ABKC);
        debugcase(OPR_CASE, AeBx);
        debugcase(OPR_EQ, ABC);
        debugcase(OPR_NE, ABC);
        debugcase(OPR_LT, ABC);
        debugcase(OPR_LE, ABC);
        debugcase(OPR_GT, ABC);
        debugcase(OPR_GE, ABC);
        debugcase(OPR_ADD, ABC);
        debugcase(OPR_SUB, ABC);
        debugcase(OPR_MUL, ABC);
        debugcase(OPR_DIV, ABC);
        debugcase(OPR_MOD, ABC);
        debugcase(OPR_POW, ABC);
        debugcase(OPR_NEG, AB);
        debugcase(OPR_NOT, AB);
        debugcase(OPR_PRINT, A);
        debugcase(OPR_PRINTX, Ax);
        debugcase(OPR_JUMP, sAx);
        debugcase(OPR_JUMPONFALSE, AeBx);
        debugcase(OPR_BUILDSTR, AB);
        debugcase(OPR_CALL, AB);
        debugcase(OPR_INVOKE, ABKC);
        debugcase(OPR_SUPERINVOKE, ABCX);
        debugcase(OPR_CLOSURE, AeBxK);
        debugcase(OPR_CLOSEUPVAL, A);
        debugcase(OPR_RETURN, Aflag);
        debugcase(OPR_CLASS, ABKC);
        debugcase(OPR_EXTRAARG, Ax);
    }

#undef debugcase

    printf("%-16s %d %d %d (flag = %s)\n", "<unknown>", A(i), B(i), C(i),
           flag(i) ? "true" : "false");
    return false;
}
