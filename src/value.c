#include "common.h"

void initValueArray(ValueArray* array) {
    array->values = NULL;
    array->capacity = 0;
    array->count = 0;
}

void writeValueArray(ValueArray* array, BagelValue value) {
    if (array->capacity < array->count + 1) {
        int oldCapacity = array->capacity;
        array->capacity = GROW_CAPACITY(oldCapacity);
        array->values = GROW_ARRAY(BagelValue, array->values, oldCapacity,
                                   array->capacity);
    }

    array->values[array->count] = value;
    array->count++;
}

void freeValueArray(ValueArray* array) {
    FREE_ARRAY(BagelValue, array->values, array->capacity);
    initValueArray(array);
}

ObjString* toString(BagelValue value) {
    switch (value.type) {
        case VAL_BOOL:
            if (AS_BOOL(value)) {
                return copyString("true", 4);
            } else {
                return copyString("false", 5);
            }
        case VAL_NIL:
            return copyString("nil", 3);
        case VAL_NUMBER: {
            char buf[40]; // No number should ever need this much
            int length = sprintf(buf, "%g", AS_NUMBER(value));
            return copyString(buf, length);
        }
        case VAL_OBJ:
            return objectToString(value);
        default:
            // unreachable
            return NULL;
    }
}

void printValue(BagelValue value) {
    switch (value.type) {
        case VAL_BOOL:
            printf(AS_BOOL(value) ? "true" : "false");
            break;
        case VAL_NIL:
            printf("nil");
            break;
        case VAL_NUMBER:
            printf("%g", AS_NUMBER(value));
            break;
        case VAL_OBJ:
            printObject(value);
            break;
        case VAL_ERROR:
            break;
    }
}

bool valuesEqual(BagelValue a, BagelValue b) {
    if (a.type != b.type) {
        return false;
    }
    switch (a.type) {
        case VAL_BOOL:
            return AS_BOOL(a) == AS_BOOL(b);
        case VAL_NIL:
            return true;
        case VAL_NUMBER:
            return AS_NUMBER(a) == AS_NUMBER(b);
        case VAL_OBJ:
            return AS_OBJ(a) == AS_OBJ(b);
        default:
            return false; // Unreachable
    }
}
