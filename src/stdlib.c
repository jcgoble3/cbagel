#include <time.h>

#include "bagel.h"

static BagelValue clockNative(int argCount, BagelValue* args) {
    return bagel_double((double)clock() / CLOCKS_PER_SEC);
}

static BagelValue errorNative(int argCount, BagelValue* args) {
    if (bagel_type(args[0]) != BAGEL_TSTRING) {
        return bagel_argerror(1, BAGEL_TSTRING);
    }
    return bagel_error(bagel_tostring(args[0]));
}

static BagelValue getFieldNative(int argCount, BagelValue* args) {
    if (bagel_type(args[0]) != BAGEL_TINSTANCE) {
        return bagel_argerror(1, BAGEL_TINSTANCE);
    }
    if (bagel_type(args[1]) != BAGEL_TSTRING) {
        return bagel_argerror(2, BAGEL_TSTRING);
    }
    return bagel_getfield(args[0], bagel_tostring(args[1]));
}

static BagelValue setFieldNative(int argCount, BagelValue* args) {
    if (bagel_type(args[0]) != BAGEL_TINSTANCE) {
        return bagel_argerror(1, BAGEL_TINSTANCE);
    }
    if (bagel_type(args[1]) != BAGEL_TSTRING) {
        return bagel_argerror(2, BAGEL_TSTRING);
    }
    bagel_setfield(args[0], bagel_tostring(args[1]), args[2]);
    return BAGEL_NIL;
}

static BagelValue hasFieldNative(int argCount, BagelValue* args) {
    if (bagel_type(args[0]) != BAGEL_TINSTANCE) {
        return bagel_argerror(1, BAGEL_TINSTANCE);
    }
    if (bagel_type(args[1]) != BAGEL_TSTRING) {
        return bagel_argerror(2, BAGEL_TSTRING);
    }
    return bagel_bool(bagel_hasfield(args[0], bagel_tostring(args[1])));
}

static BagelValue deleteFieldNative(int argCount, BagelValue* args) {
    if (bagel_type(args[0]) != BAGEL_TINSTANCE) {
        return bagel_argerror(1, BAGEL_TINSTANCE);
    }
    if (bagel_type(args[1]) != BAGEL_TSTRING) {
        return bagel_argerror(2, BAGEL_TSTRING);
    }
    if (bagel_deletefield(args[0], bagel_tostring(args[1]))) {
        return BAGEL_NIL;
    } else {
        return bagel_error("Undefined property '%s'.",
                           bagel_tostring(args[1]));
    }
}

extern void bagel_loadstdlib(void) {
    bagel_setcfunc("clock", clockNative, 0);
    bagel_setcfunc("error", errorNative, 1);
    bagel_setcfunc("getField", getFieldNative, 2);
    bagel_setcfunc("setField", setFieldNative, 3);
    bagel_setcfunc("hasField", hasFieldNative, 2);
    bagel_setcfunc("deleteField", deleteFieldNative, 2);
}
