#ifndef bagel_chunk_h
#define bagel_chunk_h

typedef enum OpCodeReg
{
    OPR_MOVE,
    OPR_LOADK,
    OPR_LOADI,
    OPR_LOADNIL,
    OPR_LOADBOOL,
    OPR_GETGLOBAL,
    OPR_SETGLOBAL,
    OPR_DEFGLOBAL,
    OPR_GETUPVAL,
    OPR_SETUPVAL,
    OPR_GETATTR,
    OPR_SETATTR,
    OPR_GETSUPER,
    OPR_CASE,
    OPR_EQ,
    OPR_NE,
    OPR_LT,
    OPR_LE,
    OPR_GT,
    OPR_GE,
    OPR_ADD,
    OPR_SUB,
    OPR_MUL,
    OPR_DIV,
    OPR_MOD,
    OPR_POW,
    OPR_NEG,
    OPR_NOT,
    OPR_PRINT,
    OPR_PRINTX,
    OPR_JUMP,
    OPR_JUMPONFALSE,
    OPR_BUILDSTR,
    OPR_CALL,
    OPR_INVOKE,
    OPR_SUPERINVOKE,
    OPR_CLOSURE,
    OPR_CLOSEUPVAL,
    OPR_RETURN,
    OPR_CLASS,
    OPR_EXTRAARG,
} OpCodeReg;

typedef struct Instruction {
    u8 u[4];
} Instruction;

#define opmask   0x7F
#define flagmask 0x80

#define opflag(i) ((i).u[0])
#define op(i)     (opflag(i) & opmask)
#define flag(i)   ((bool)(opflag(i) & flagmask))
#define A(i)      ((i).u[1])
#define sA(i)     ((i8)A(i))
#define B(i)      ((i).u[2])
#define sB(i)     ((i8)B(i))
#define C(i)      ((i).u[3])

static inline i32 Bx(Instruction i) {
    return (B(i) << 8) | C(i);
}

static inline i32 sBx(Instruction i) {
    return (sB(i) << 8) | C(i);
}

static inline i32 Ax(Instruction i) {
    return (A(i) << 16) | (B(i) << 8) | C(i);
}

static inline i32 sAx(Instruction i) {
    return (sA(i) << 16) | (B(i) << 8) | C(i);
}

#define set_opf(i, op, flag) (opflag(i) = ((flag) ? (op) | flagmask : (op)))
#define set_op(i, op)        set_opf(i, op, false)

#define mask(byte)     ((u8)((byte)&0xFF))
#define set_A(i, arg)  (A(i) = mask(arg))
#define set_B(i, arg)  (B(i) = mask(arg))
#define set_C(i, arg)  (C(i) = mask(arg))
#define set_Bx(i, arg) (B(i) = mask(arg >> 8), set_C(i, arg))
#define set_Ax(i, arg) (A(i) = mask(arg >> 16), set_Bx(i, arg))

// Limits imposed by the bytecode format

// maximum registers per function
#define MAXREG UINT8_MAX
// invalid register
#define NOREG UINT8_MAX
// maximum upvalues per function
#define MAXUPVALUES (UINT8_MAX + 1)
// maximum constants per function
#define MAXCONSTANT (1 << 24)
// maximum jump distance
#define MAXJUMP ((1 << 23) - 1)
// placeholder for unpatched jump
#define PLACEHOLDER (-(1 << 23))

typedef struct Chunk {
    int count;
    int capacity;
    Instruction* code;
    int linecount;
    int linecapacity;
    int* lines;
    int* lineruns;
    ValueArray constants;
    Table constantTable;
} Chunk;

void initChunk(Chunk* chunk);
void freeChunk(Chunk* chunk);

void writeABCf(Chunk* chunk, int line, OpCodeReg o, u8 a, u8 b, u8 c, bool f);
void writeA(Chunk* chunk, int line, OpCodeReg o, u8 a);
void writeAflag(Chunk* chunk, int line, OpCodeReg o, u8 a, bool f);
void writeAB(Chunk* chunk, int line, OpCodeReg o, u8 a, u8 b);
void writeABC(Chunk* chunk, int line, OpCodeReg o, u8 a, u8 b, u8 c);
void writeABCX(Chunk* chunk, int line, OpCodeReg o, u8 a, u8 b, u8 c,
               u32 extra);
void writeAeBx(Chunk* chunk, int line, OpCodeReg o, u8 a, u32 ebx);
void writeAsBx(Chunk* chunk, int line, OpCodeReg o, u8 a, i16 sbx);
void writeAx(Chunk* chunk, int line, OpCodeReg o, u32 ax);
void writesAx(Chunk* chunk, int line, OpCodeReg o, i32 sax);
void writeInstruction(Chunk* chunk, int line, Instruction i);
int addConstant(Chunk* chunk, BagelValue value);
int getLine(Chunk* chunk, int codeindex);

#endif // include guard
