#ifndef bagel_table_h
#define bagel_table_h

typedef struct {
    ObjString* key;
    BagelValue value;
} Entry;

typedef struct {
    int count;
    int capacity;
    Entry* entries;
} Table;

void initTable(Table* table);
void freeTable(Table* table);
bool tableGet(Table* table, ObjString* key, BagelValue* value);
bool tableSet(Table* table, ObjString* key, BagelValue value);
bool tableDelete(Table* table, ObjString* key);
void tableAddAll(Table* from, Table* to);
ObjString* tableFindString(Table* table, const char* chars, int length,
                           uint32_t hash);
void tableRemoveWhite(Table* table);
void markTable(Table* table);

#endif // include guard
