#ifndef bagel_vm_h
#define bagel_vm_h

#define FRAMES_MAX 64
#define STACK_MAX  (FRAMES_MAX * UINT8_COUNT)

typedef struct {
    ObjClosure* closure;
    Instruction* ip;
    BagelValue* slots;
} CallFrame;

typedef struct {
    CallFrame frames[FRAMES_MAX];
    int frameCount;

    BagelValue stack[STACK_MAX];
    BagelValue* stackTop;
    BagelValue* lastStackTop;
    BagelValue tempRoot[10];
    BagelValue* tempTop;
    Table globals;
    Table strings;
    ObjString* initString;
    ObjUpvalue* openUpvalues;

    size_t bytesAllocated;
    size_t nextGC;

    Obj* objects;
    int grayCount;
    int grayCapacity;
    Obj** grayStack;
} VM;

typedef enum
{
    INTERPRET_OK,
    INTERPRET_COMPILE_ERROR,
    INTERPRET_RUNTIME_ERROR,
} InterpretResult;

extern VM vm;

void runtimeError(const char* format, va_list args);
void initVM();
void freeVM();
InterpretResult interpret(const char* source);
void pushTemp(BagelValue value);
void popTemp(int n);
BagelValue argerror(int arity, int actual);
bool bindMethod(ObjClass* klass, ObjString* name, BagelValue* dest);
bool isFalsey(BagelValue value);
BagelValue run(void);

#endif // include guard
