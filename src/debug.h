#ifndef bagel_debug_h
#define bagel_debug_h

void disassembleChunk(Chunk* chunk, const char* name);
int disassembleInstruction(Chunk* chunk, int offset, int line);

#endif // include guard
