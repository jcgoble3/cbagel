# Chapter 15: A Virtual Machine

- [Link to book chapter](https://craftinginterpreters.com/a-virtual-machine.html)
- [Previous chapter](14-chunks-of-bytecode.md)
- [Table of Contents](index.md)
- [Next chapter](16-scanning-on-demand.md)

## 15.1: An Instruction Execution Machine

- Bytecode is executed by a *virtual machine* or VM
- VM tracks where it is via an *instruction pointer* (IP)
  - IP always points to the next instruction, not the one currently being handled
- `run()` is what actually executes the bytecode
  - 90% of clox's execution time is spent here
  - First figure out what opcode we're looking at, then dispatch to the matching C code
    - Dispatch is the most performance-critical piece of code in the VM
    - clox uses a single giant `switch` statement for simplicity
    - Other faster solutions require non-standard C or handwritten assembly
- VMs tend to be a "black box" and a pain to debug, so debug tracing code
  is essential for understanding what's going on

## 15.2: A Value Stack Manipulator

- Evaulating a statement like `var x = y - 3` needs several instructions:
  - Look up `y`
  - Load constant `3`
  - Subtract
  - Store into `x`
- How do the subtraction and store instructions know what to subtract or store?
  Answer: they operate on a *stack*.
- A stack works for temporary storage of values on the VM because a value evaluated
  before a second value will always live at least as long as that second value
- This means that the first value produced is the last to be consumed
- First in, last out implies a stack
- Stack-based VMs make compiling and executing code simple, yet fast enough
  to be used by major languages such as Python
- Like the IP, the stack top pointer points just past the actual top element
- The stack also needs debug tracing to help understand what's happening

## 15.3: An Arithmetic Calculator

- When writing a multi-statement macro, use a `do-while false` loop to avoid errors
- Binary operation instructions need to pop the right operand first because
  it was pushed after the left operand
- Not much else; this section is mostly hands-on implementation and light on theory

## Design Note: Register-Based Bytecode

- *Register-based* is another family of bytecode architectures
- No relation to hardware chip registers
- Register-based VMs also use a stack, but allow instructions to
  access any slot, not just the top slot(s)
- In a stack-based VM, adding two values and storing the result into a variable
  requires four instructions: two loads, the addition, and the store,
  adding up to seven bytes of code
- In a register-based VM, the same statement would compile to a single add instruction
  with three operands, namely the slots of the two operands and where to put the result,
  adding up to just four bytes of code
- The register-based version is faster (one decode and dispatch instead of four) and smaller
- Lua was one of the first major languages to switch to a register-based VM with Lua 5.0
- Stack-based VMs remain popular for simplicity, as register-based VMs are significantly
  harder to implement, particularly with regard to compiling to the register-based bytecode
