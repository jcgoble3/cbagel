# Chapter 24: Calls and Functions

- [Link to book chapter](https://craftinginterpreters.com/calls-and-functions.html)
- [Previous chapter](23-jumping-back-and-forth.md)
- [Table of Contents](index.md)
- [Next chapter](25-closures.md)

## 24.1: Function Objects

- Each function in clox has its own `Chunk`
- Functions in Lox are first-class values, so they get the same `Obj` treatment
  that strings do

## 24.2: Compiling to Function Objects

- The compiler needs to know which function to output bytecode into but also
  needs to handle top-level code not in a function
- The solution in clox is to automatically wrap that top-level code in an
  implicit function, so that everything is in a function
  - This is similar to Lua where every source file is treated as a vararg function

## 24.3: Call Frames

- Local variables need to be specific to the function *invocation*, not just to
  the function object, because recursion is a thing
- Locals are still first-in-last-out even when function calls are added, so we
  can still continue to keep them on the stack
- However, the compiler can no longer determine the absolute index of every local
  variable at compile-time because a function doesn't know the size of the stack
  below the call
- The *relative* locations of local variables, however, are still fixed within a
  function at compile-time
- So a function's local variables live in fixed stack slots relative to the
  starting point of the function call
- Therefore the VM will record at each call the current stack top and add that
  to the relative stack indices within that function -- known as a **call frame**
- We also need to track where in the caller execution should resume when a function
  call completes -- the return address

## 24.4: Function Declarations

- Each function body has its own compiler struct, since each one is a separate chunk
- These compilers form a stack, implemented as a linked list that threads through
  the C stack, since each one is a local variable in a C function
- Parameters are simply local variables and compiled as such

## 24.5: Function Calls

- When a function is called, the top section of the stack consists of the function
  being called with all arguments on top of it
- To execute a function, we need the function object at stack index 0 and its
  parameters above it
- This means we can just have overlapping call frames; the call just creates a new
  call frames that positions its stack bottom at the function being called

## 24.6: Return Statements

- Nothing special here, just simple implementation

## 24.7: Native Functions

- A programming language needs a standard library to be of any use
- Lox is as bare bones as it gets: just one library function
- Native functions require different machinery than Lox functions, so are treated
  as a different object type
- `defineNative()` assumes that the stack is completely empty; to support loading
  of modules written in C at runtime, this assumption would need to be removed
  - In fact, this would require a full public API
