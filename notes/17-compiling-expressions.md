# Chapter 17: Compiling Expressions

- [Link to book chapter](https://craftinginterpreters.com/compiling-expressions.html)
- [Previous chapter](16-scanning-on-demand.md)
- [Table of Contents](index.md)
- [Next chapter](18-types-of-values.md)

## Lede

- clox uses a **Pratt parser** for expressions
  - Named after Vaughan Pratt, also known as "top-down operator precedence parsing"
  - Often used in production compilers but hard to find in books
  - Pratt parsing relies on a big table of data, making it hard to break into pieces

## 17.1: Single-Pass Compilation

- The two jobs of a compiler:
  - Parse source code to understand what it means
  - Translate that knowledge into low-level instructions
- In many languages (like Python), those are two separate passes:
  - A parser produces an abstract syntax tree (AST)
  - A code generator walks the AST and emits bytecode or machine code
- clox, like Lua, uses a single pass, which keeps the compiler simpler
  - Single-pass compilation requires that the language not need much context
    to understand a particular token

## 17.2: Parsing Tokens

- This section is just setting up the parser, light on theory
- Code does not compile after this section because of undefined `expression()` function

## 17.3: Emitting Bytecode

- Mostly setting up the code generation framework
- User-defined functions will be separate chunks
- Code still does not compile (same reason)

## 17.4: Parsing Prefix Expressions

- This chapter only deals with number literals, grouping parentheses, unary negation,
  and the four basic arithmetic operations (or, as the book calls them,
  The Four Horsemen of the Arithmetic)
- Each token type maps to an expression type
- Eaach expression type gets a function that emits the right bytecode
- Those functions are placed in an array, with indexes matching token type enum values
- Parentheses are purely syntactic and have no runtime semantics
- Precedence is handled by each expression type recursively parsing an expression
  of a particular precedence level or higher
- Code now compiles (with the addition of two forward declarations from 17.6),
  but does nothing useful

## 17.5: Parsing Infix Expressions

- The array of functions is actually a *table* with multiple columns
  - One column is prefix parser functions, the second column is infix parser functions
- To parse a *left*-associative operator, the right operand has a precedence level
  one higher than the oparator
- To parse a *right*-associative operator, the right operand has the *same* precedence
  level as the operator

## 17.6 A Pratt Parser

- The C syntax for the type of a pointer to a function is so horribly bad that it
  makes me want to throw myself off a cliff... or at least hide it behind a `typedef`
- The function table also has a third column for the precedence of that operator as
  an infix expression
- The first token of an expression is always going to be some sort of prefix expression
- Then we loop until we leave the expression completely or encounter a lower precedence operator

## 17.7 Dumping Chunks

- Debug code to inspect the guts of the compiler is always helpful.
