# Chapter 22: Local Variables

- [Link to book chapter](https://craftinginterpreters.com/local-variables.html)
- [Previous chapter](21-global-variables.md)
- [Table of Contents](index.md)
- [Next chapter](23-jumping-back-and-forth.md)

## 22.1: Representing Local Variables

- Unlike global variables, which are stored in a hash table and resolved
  at runtime, Lox keeps local variables on the stack and resolves them at
  compile time.

## 22.2: Block Statements

- A block creates a new scope for local variables

## 22.3: Declaring Local Variables

- We have to check if a local variable already exists in the current scope,
  because Lox does not allow shadowing locals in the same scope
- We also have to pop the values from the stack at the end of the scope

## 22.4: Using Locals

- To resolve a local, we walk the locals array backward (to find the *last*
  declared variable) and return the stack index of it, or a flag to signal
  that it should be treated as a global
- Conveniently, the index in the locals array is also *exactly* the index
  of the variable on the stack, since all statements (other than declaring
  a local) balance their stack usage evenly
- However, this implementation means that local variables names are not known
  at runtime, which is bad for introspection and debugging
